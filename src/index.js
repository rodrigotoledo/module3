/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { View } from 'react-native';
import Routes from 'routes';


import 'config/ReactotronConfig';
import store from 'store';

export default class App extends Component {
  render() {
    return <Provider store={store}>
        <Routes />
      </Provider>;
  }
}
