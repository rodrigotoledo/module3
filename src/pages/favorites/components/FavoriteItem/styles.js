import { StyleSheet } from 'react-native';
import { colors, metrics } from 'styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: metrics.baseMargin,
    borderRadius: metrics.baseRadius,
    marginTop: metrics.baseMargin,
    marginHorizontal: metrics.baseMargin,
    flexDirection: "row",
    alignItems: "center"
  },

  avatar: {
    width: 54,
    height: 54,
    margin: metrics.smallMargin
  },

  title: {
    fontWeight: 'bold',
    fontSize: 14,
  }
});

export default styles;
