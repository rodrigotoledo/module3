import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";

import sagas from './sagas';
import reducers from './reducers';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
    sagaMiddleware
];

const createApproprietaStore = __DEV__ ? console.tron.createStore : createStore;

const store = createApproprietaStore(reducers, applyMiddleware(...middlewares));

sagaMiddleware.run(sagas);

export default store;
